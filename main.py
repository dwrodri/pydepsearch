import gzip
import json
import time
from functools import lru_cache
from typing import Callable, SupportsFloat, cast

import numpy as np
import pandas as pd
from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from rapidfuzz import process

app = FastAPI()

PYPI_PROJECT_PREFIX = "https://pypi.org/project/"


def log_monotonic(fn: Callable):
    """
    decorator for logging function call duration
    """

    def wrapper(*args, **kwargs):
        start = time.monotonic()
        result = fn(*args, **kwargs)
        print(f"{fn.__name__} took {time.monotonic() - start: .3f} seconds")
        return result

    return wrapper


@log_monotonic
def load_pkg_list_from_bq_data(
    filename: str = "PyPI_dl_results.json.gz",
) -> pd.DataFrame:
    data = {"num_downloads": [], "project": []}
    with gzip.open(filename, "rt") as fp:
        for line in fp:
            row = json.loads(line)
            data["num_downloads"].append(int(row["num_downloads"]))
            data["project"].append(row["project"])
    return pd.DataFrame(data)


PKG_DF = load_pkg_list_from_bq_data()


@log_monotonic
@lru_cache(maxsize=10_000)
def simple_search(query: str) -> list[str]:
    """
    combine fuzzy matching with log-scale download count to score query
    """
    global PKG_DF
    fuzzy_results = cast(
        list[tuple[str, SupportsFloat, int]],
        process.extract(
            query,
            {i: PKG_DF["project"][i] for i in PKG_DF.index},
            score_cutoff=20,
            limit=25,
        ),
    )
    temp_df: pd.DataFrame = PKG_DF.iloc[[elem[-1] for elem in fuzzy_results]].copy()  # type: ignore
    temp_df["fuzzy_score"] = [elem[1] for elem in fuzzy_results]
    temp_df["final_score"] = temp_df["fuzzy_score"] * temp_df["num_downloads"].astype(
        "float64"
    ).apply(np.log)
    return temp_df.sort_values("final_score")["project"].tolist()


@app.get("/", response_class=HTMLResponse)
async def return_homepage():
    with open("homepage.html") as fp:
        return "".join(fp.readlines())


@app.get("/search/")
async def query(
    query: str,
):
    """
    Lookup packages.
    """
    global PKG_DF
    results = simple_search(query)
    for i in range(len(results)):
        if query == results[i]:
            results = [query] + results[:i] + results[i + 1 :]
    return {"results": [PYPI_PROJECT_PREFIX + result for result in results]}
