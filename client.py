import sys
import requests
from urllib.parse import quote as encode

for url in requests.get(
    "http://pydepsearch.dwrodri.com/search/",
    params={"query": encode(" ".join(sys.argv[1:]))},
).json()["results"]:
    print(url)
