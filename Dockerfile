FROM python:3.9-slim-buster


ADD . /app/
WORKDIR /app/
RUN pip install -r requirements.txt
EXPOSE 80/tcp
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]