# pydepsearch

This is the source code for the client and the server of PyDepSearch, a backup
search index for pyPI that I put together when `pip search` went down.

The API is hosted on a tiny droplet for now, let's see if it gets any traffic!

The client is so simple, I can embed the whole thing right here:
```python
import sys
import requests
from urllib.parse import quote as encode

for url in requests.get(
    "http://pydepsearch.dwrodri.com/search/",
    params={"query": encode(" ".join(sys.argv[1:]))},
).json()["results"]:
    print(url)
```

I need to figure out the whole PyPI thing so that I can get this hosted!

Many thanks to the maintainers of all the dependencies who allowed for me to
write this in 5 hours because I couldn't sleep.